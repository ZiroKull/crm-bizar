<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('barcode')->after('category_id')->unique()->nullable();
            $table->bigInteger('max_stock')->after('barcode')->nullable();
            $table->bigInteger('min_stock')->after('max_stock')->nullable();
            $table->bigInteger('stock')->after('min_stock')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('instock_matrix');
        });
    }
}
