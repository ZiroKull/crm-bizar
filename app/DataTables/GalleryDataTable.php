<?php

namespace App\DataTables;

use App\Models\Gallery;
use App\Models\CustomField;
use App\Models\Product;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Barryvdh\DomPDF\Facade as PDF;

class GalleryDataTable extends DataTable
{
    /**
     * custom fields columns
     * @var array
     */
    public static $customFields = [];

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $columns = array_column($this->getColumns(), 'data');
        $dataTable = $dataTable
            ->editColumn('min_stock', function ($product) {
                return $product['min_stock'] ? $product['min_stock'] : 0 ;
            })
            ->editColumn('max_stock', function ($product) {
                return $product['max_stock'] ? $product['max_stock'] : 0 ;
            })
            ->editColumn('stock', function ($product) {
                return $product['stock'] ? $product['stock'] : 0 ;
            })
            ->editColumn('orders', function ($product) {
                return getOrderColumn($product);
            })
            ->rawColumns(array_merge($columns, ['action']));

        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        if (auth()->user()->hasRole('admin')) {
            return $model->newQuery();
        } else {
            return $model->newQuery()->with("market")
                ->join("user_markets", "user_markets.market_id", "=", "galleries.market_id")
                ->where('user_markets.user_id', auth()->id())
                ->select('galleries.*');
        }

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            [
                'data' => 'name',
                'title' => trans('lang.gallery_name'),

            ],
            [
                'data' => 'min_stock',
                'title' => trans('lang.gallery_stock_min'),

            ],
            [
                'data' => 'max_stock',
                'title' => trans('lang.gallery_stock_max'),

            ],
            [
                'data' => 'stock',
                'title' => trans('lang.gallery_stock'),

            ],
            [
                'data' => 'orders',
                'title' => trans('lang.gallery_order'),
                'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false,

            ]
        ];

        $hasCustomField = in_array(Product::class, setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFieldsCollection = CustomField::where('custom_field_model', Product::class)->where('in_table', '=', true)->get();
            foreach ($customFieldsCollection as $key => $field) {
                array_splice($columns, $field->order - 1, 0, [[
                    'data' => 'custom_fields.' . $field->name . '.view',
                    'title' => trans('lang.gallery_' . $field->name),
                    'orderable' => false,
                    'searchable' => false,
                ]]);
            }
        }
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'galleriesdatatable_' . time();
    }

    /**
     * Export PDF using DOMPDF
     * @return mixed
     */
    public function pdf()
    {
        $data = $this->getDataForPrint();
        $pdf = PDF::loadView($this->printPreview, compact('data'));
        return $pdf->download($this->filename() . '.pdf');
    }
}