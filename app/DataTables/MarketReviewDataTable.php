<?php
/**
 * File name: MarketReviewDataTable.php
 * Last modified: 2020.05.04 at 09:04:19
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\DataTables;

use App\Models\CustomField;
use App\Models\Order;
use App\Models\Product;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

/**
 * Class MarketReviewDataTable
 * @package App\DataTables
 */
class MarketReviewDataTable extends DataTable
{
    /**
     * custom fields columns
     * @var array
     */
    public static $customFields = [];

    public function ajax()
    {
        $query = null;
        if (method_exists($this, 'query')) {
            $query = app()->call([$this, 'query']);
            $query = $this->applyScopes($query);
        }

        /** @var \Yajra\DataTables\DataTableAbstract $dataTable */
        $dataTable = app()->call([$this, 'dataTable'], compact('query'));

        if ($callback = $this->beforeCallback) {
            $callback($dataTable);
        }

        if ($callback = $this->responseCallback) {
            $data = new Collection($dataTable->toArray());

            return new JsonResponse($callback($data));
        }

        return $dataTable->toJson();
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $columns = array_column($this->getColumns(), 'data');
        $dataTable = $dataTable
            ->editColumn('name', function ($product) {
                return $product->name;
            })
            ->editColumn('quantity', function ($product) {
                $subtotal = 0;
                foreach ($product->productOrders as $productOrder) {
                    $subtotal += $productOrder->quantity;
                }
                return $subtotal;
            })
            ->editColumn('summ', function ($product) {
                $sum = 0;
                foreach ($product->productOrders as $productOrder) {
                    foreach ($productOrder->options as $option) {
                        $productOrder->price += $option->price;
                    }
                    $sum += $productOrder->price * $productOrder->quantity;
                }
                return getNumerFormat($sum);
            })
            ->editColumn('cost', function ($product) {
                $subtotal = 0;
                foreach ($product->productOrders as $productOrder) {
                    $subtotal += $productOrder->quantity;
                }
                $cost = $subtotal * $product->cost_price;
                return getNumerFormat($cost);
            })
            ->editColumn('profit', function ($product) {
                $sum = 0;
                $subtotal = 0;
                foreach ($product->productOrders as $productOrder) {
                    foreach ($productOrder->options as $option) {
                        $productOrder->price += $option->price;
                    }
                    $sum += $productOrder->price * $productOrder->quantity;
                    $subtotal += $productOrder->quantity;
                }
                $cost = $subtotal * $product->cost_price;
                $prof = $sum - $cost;
                return getNumerFormat($prof);
            })
            ->editColumn('marginality', function ($product) {
                $sum      = 0;
                $subtotal = 0;
                $margin   = 0;
                foreach ($product->productOrders as $productOrder) {
                    foreach ($productOrder->options as $option) {
                        $productOrder->price += $option->price;
                    }
                    $sum += $productOrder->price * $productOrder->quantity;
                    $subtotal += $productOrder->quantity;
                }
                $cost = $subtotal * $product->cost_price;
                $prof = $sum - $cost;
                if ($sum !== 0) {
                    $margin = ($prof / $sum) * 100;
                }
                return getNumerFormat($margin) ;
            })
            ->rawColumns(array_merge($columns, ['action']));

        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\MarketReview $model
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function query(Product $model, Request $request)
    {
        if (isset($request->from) && isset($request->to))
        {
            if (auth()->user()->hasRole('admin')) {
                return $model->newQuery()->with(['productOrders' => function($query) {
                    $query->whereBetween('created_at', [
                        $this->request->from,
                        $this->request->to
                    ]);
                }]);
            } else if (auth()->user()->hasRole('manager')) {
                return $model->newQuery()->with("market")->with("category")
                    ->with(['productOrders' => function($query) {
                        $query->whereBetween('created_at', [
                            $this->request->from,
                            $this->request->to
                        ]);
                    }])
                    ->join("user_markets", "user_markets.market_id", "=", "products.market_id")
                    ->where('user_markets.user_id', auth()->id())
                    ->groupBy('products.id')
                    ->select('products.*')->orderBy('products.updated_at', 'desc');
            } else if (auth()->user()->hasRole('driver')) {
                return $model->newQuery()->with("market")->with("category")
                    ->with(['productOrders' => function($query) {
                        $query->whereBetween('created_at', [
                            $this->request->from,
                            $this->request->to
                        ]);
                    }])
                    ->join("driver_markets", "driver_markets.market_id", "=", "products.market_id")
                    ->where('driver_markets.user_id', auth()->id())
                    ->groupBy('products.id')
                    ->select('products.*')->orderBy('products.updated_at', 'desc');
            } else if (auth()->user()->hasRole('client')) {
                return $model->newQuery()->with("market")->with("category")
                    ->with(['productOrders' => function($query) {
                        $query->whereBetween('created_at', [
                            $this->request->from,
                            $this->request->to
                        ]);
                    }])
                    ->join("product_orders", "product_orders.product_id", "=", "products.id")
                    ->join("orders", "product_orders.order_id", "=", "orders.id")
                    ->where('orders.user_id', auth()->id())
                    ->groupBy('products.id')
                    ->select('products.*')->orderBy('products.updated_at', 'desc');
            }
        } else {
            if (auth()->user()->hasRole('admin')) {
                return $model->newQuery();
            } else if (auth()->user()->hasRole('manager')) {
                return $model->newQuery()->with("market")->with("category")
                    ->join("user_markets", "user_markets.market_id", "=", "products.market_id")
                    ->where('user_markets.user_id', auth()->id())
                    ->groupBy('products.id')
                    ->select('products.*')->orderBy('products.updated_at', 'desc');
            } else if (auth()->user()->hasRole('driver')) {
                return $model->newQuery()->with("market")->with("category")
                    ->join("driver_markets", "driver_markets.market_id", "=", "products.market_id")
                    ->where('driver_markets.user_id', auth()->id())
                    ->groupBy('products.id')
                    ->select('products.*')->orderBy('products.updated_at', 'desc');
            } else if (auth()->user()->hasRole('client')) {
                return $model->newQuery()->with("market")->with("category")
                    ->join("product_orders", "product_orders.product_id", "=", "products.id")
                    ->join("orders", "product_orders.order_id", "=", "orders.id")
                    ->where('orders.user_id', auth()->id())
                    ->groupBy('products.id')
                    ->select('products.*')->orderBy('products.updated_at', 'desc');
            }
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax([
                'url' => route('marketReviews.index'),
                'type' => 'GET',
                'data' => 'function(d) {
                    d.from = $(".data-start").val(),
                    d.to   = $(".data-end").val()
                 }'
            ])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            [
                'data' => 'name',
                'title' => trans('lang.market_review_name'),

            ],
            [
                'data' => 'quantity',
                'title' => trans('lang.market_review_quantity'),
                'searchable' => false, 'orderable' => false, 'exportable' => true, 'printable' => true,
            ],
            [
                'data' => 'summ',
                'title' => trans('lang.market_review_summ'),
                'searchable' => false, 'orderable' => false, 'exportable' => true, 'printable' => true,

            ],
            [
                'data' => 'cost',
                'title' => trans('lang.market_review_cost_price'),
                'searchable' => false, 'orderable' => false, 'exportable' => true, 'printable' => true,
            ],
            [
                'data' => 'profit',
                'title' => trans('lang.market_review_cost_profit'),
                'searchable' => false, 'orderable' => false, 'exportable' => true, 'printable' => true,

            ],
            [
                'data' => 'marginality',
                'title' => trans('lang.market_review_marginality'),
                'searchable' => false, 'orderable' => false, 'exportable' => true, 'printable' => true,

            ]
        ];

        $hasCustomField = in_array(Order::class, setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFieldsCollection = CustomField::where('custom_field_model', Order::class)->where('in_table', '=', true)->get();
            foreach ($customFieldsCollection as $key => $field) {
                array_splice($columns, $field->order - 1, 0, [[
                    'data' => 'custom_fields.' . $field->name . '.view',
                    'title' => trans('lang.order_' . $field->name),
                    'orderable' => false,
                    'searchable' => false,
                ]]);
            }
        }
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'market_reviewsdatatable_' . time();
    }

    /**
     * Export PDF using DOMPDF
     * @return mixed
     */
    public function pdf()
    {
        $data = $this->getDataForPrint();
        $pdf = PDF::loadView($this->printPreview, compact('data'));
        return $pdf->download($this->filename() . '.pdf');
    }
}