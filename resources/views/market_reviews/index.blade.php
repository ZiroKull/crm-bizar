@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
  <div class="clearfix"></div>
   <!-- /.box -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Продажи на {{ date('d.m.Y') }} {{ date('H:i:s') }} </h3>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                <li class="nav-item mr-2 mb-3">
                    {!! Form::text('data_start', null,  ['class' => 'form-control data-start datepicker','placeholder'=>  trans("lang.market_reviews_data_start")]) !!}
                </li>
                <li class="nav-item mr-2 mb-3">
                    {!! Form::text('data_end', null,  ['class' => 'form-control data-end datepicker','placeholder'=>  trans("lang.market_reviews_data_end")]) !!}
                </li>
                <li class="nav-item mr-2 mb-3">
                    <button type="submit" class="btn btn-{{setting('theme_color')}} sort-data"> {{trans('lang.market_reviews_show')}} </button>
                </li>
                <li class="nav-item mr-2 mb-3">
                    <button type="submit" class="btn btn-{{setting('theme_color')}} reset-data"> {{trans('lang.market_reviews_reset')}} </button>
                </li>
                @include('layouts.right_toolbar', compact('dataTable'))
            </ul>
        </div>
        <div class="card-body">
            @include('market_reviews.table')
            <div class="clearfix"></div>
        </div> <!-- /.card-body -->
    </div>
</div>
@endsection

