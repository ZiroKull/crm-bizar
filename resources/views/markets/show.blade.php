@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col">
                    <h3 class="box-title">Показатели датчиков микромаркета {{  $market->name }} на {{ date('d.m.Y') }} {{ date('H:i:s') }} </h3>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="content">
        <div class="clearfix"></div>
        <!-- /.box -->

        <div class="box">
            <div class="box-header"></div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::hidden('host', env("MQTT_HOST") , ['class' => 'host']) !!}
                {!! Form::hidden('port', env("MQTT_PORT") , ['class' => 'port']) !!}
                {!! Form::hidden('user', env("MQTT_USER") , ['class' => 'user']) !!}
                {!! Form::hidden('pass', env("MQTT_PASS") , ['class' => 'pass']) !!}
                {!! Form::hidden('idhw', $market->idhw ? $market->idhw : "-1" , ['class' => 'idhw']) !!}
                {!! Form::hidden('clientId', $market->id , ['class' => 'clientId']) !!}
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th  width="25%"><i class="nav-icon fa fa-thermometer-half"></i> Температура, °</th>
                        <th  width="25%">Влажность, %</th>
                        <th  width="25%">Свет</th>
                        <th  width="25%">Дверь</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <span class="temp">0</span>
                        </td>
                        <td>
                            <span class="humi">0</span>
                        </td>
                        <td>
                            <label class="col-9 ml-2 form-check-inline">
                                {!! Form::checkbox('light', 1, null, ['class' => 'apple-switch light']) !!}
                            </label>
                        </td>
                        <td>
                            <label class="col-9 ml-2 form-check-inline">
                                {!! Form::checkbox('door', 1, null, ['class' => 'apple-switch door']) !!}
                            </label>
                        </td>
                    </tr>


                    </tbody>
                </table>
            </div>
        </div> <!-- /.box-body -->


    </div>
@endsection


