@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
  <div class="clearfix"></div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Товарная матрица и остатки на {{ date('d.m.Y') }} {{ date('H:i:s') }} </h3>
        </div>
        <!-- /.box-header -->
    </div> <!-- /.box-body -->
   <!-- /.box -->
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                @include('layouts.right_toolbar', compact('dataTable'))
            </ul>
        </div>
        <div class="card-body">
            @include('galleries.table')
            <div class="clearfix"></div>
        </div> <!-- /.card-body -->
    </div>
</div>
@endsection

