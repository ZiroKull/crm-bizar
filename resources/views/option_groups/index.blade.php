@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">

  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
  <div class="clearfix"></div>
   <!-- /.box -->

          <div class="box">
            <div class="box-header">
            <h3 class="box-title">Показатели датчиков микромаркета GFC на {{ date('d.m.Y') }} {{ date('H:i:s') }} </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><i class="nav-icon fa fa-thermometer-half"></i> Температура, °</th>
                  <th>Напряжеение, В</th>
                  <th>Влажность, %</th>
                  <th>Свет</th>
                  <th>Дверь</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>25</td>
                  <td>238</td>
                  <td>28</td>
                  <td>Включен</td>
                  <td>Закрыта</td>
                </tr>
                
                
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
          </div> <!-- /.box-body -->

          
</div>
@endsection


