<?php

return [
    'agree' => 'Соглашаюсь с условиями использования платформы EODA',
    'already_member' => 'Я зарегистрирован',
    'email' => 'Email адрес',
    'failed' => 'Введены неверные данныеб попробуйте еще раз',
    'forgot_password' => 'Забыли пароль?',
    'login' => 'Логин',
    'login_facebook' => '',
    'login_google' => '',
    'login_title' => 'Войдите чтобы начать работу',
    'login_twitter' => '',
    'logout' => 'Выход',
    'name' => 'Имя пользователя',
    'password' => 'Пароль',
    'password_confirmation' => 'Подтверждение пароля',
    'register' => 'Зарегистрироваться',
    'register_new_member' => 'Register a new membership',
    'remember_me' => 'Запомнить меня',
    'remember_password' => 'Вернуться ко входу',
    'reset_password' => 'Сбросить пароль',
    'reset_password_title' => 'Введите ваш новый пароль',
    'reset_title' => 'Email для сброса пароля',
    'send_password' => 'Отправить',
    'throttle' => 'Слишком много попыток входа. Попробуйте войти снова через :seconds seconds.',
];